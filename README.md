# README #

## What is this repository for? ##

**EN**: Repository contains custom location names for team overlay within Quake 3 Arena maps.

**RU**: Репозиторий содержит произвольные названия локаций для командного оверлея для карт игры Quake 3 Arena

## How do I get set up? ##

**EN**

1. Put cfg files to "<modName>/locs" directory, where "<modName>" is "osp" for example
2. Enable in console cg_customloc setting, entering in console `/cg_customloc 1`
3. Do not forget to reconnect before entering corresponding map or do in console `/vid_restart`


**RU**

1. Поместите cfg в файлы в директорию "<названиеМода>/locs", где "<названиеМода>", например, "osp"
2. Включите настройку cg_customloc в консоли: `/cg_customloc 1`
3. Не забудьте переподключиться к серверу перед использование новых названий, либо выполните в консоле `/vid_restart`
  

## Contribution guidelines ##

**EN**: you can contribute using pull requests

**RU**: предлагать сови варианты или исправления можно через механизм pull request'ов

## Creating your own locations ##

**EN**: It is easy as one two three
You need to choose place on map that you want to mark with your location by standing there and enter command on console:

`/addpos "Your custom loc name"`

File would be created if it didn't exist, otherwise it appends new mark to existing file.


**RU**: Создать и/или добавть собственное местоположение добвольно просто.
Необходимо встать на карте на точку, которую хочется отметить по-совсему и ввести команду:
`/addpos "Your custom loc name"`

Если файла с локациями не существовало, то он будет создан и туда будет добавлена точка. Если существовал такой файл, то точка добавится в этот файл.
